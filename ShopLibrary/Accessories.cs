﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopLibrary
{
    class Accessories : Product
    {
        public string Material { get; set; } //материал

        public Accessories(string material, int id, int size, double price, string country, string color)
        {
            Material = material;
            Id = id;
            Size = size;
            Price = price;
            Country = country;
            Color = color;
        }

        public Accessories()
        {
        }

        public override string ToString()
        {
            return $"Id:{Id} Материал:{Material} Price:{Price} Country:{Country} Size:{Size} Color:{Color}";
        }

    }
}
