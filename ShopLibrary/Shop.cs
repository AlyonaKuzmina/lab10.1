﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Encodings;
using System.Text.Encodings.Web;
using System.IO;
using System.Text.RegularExpressions;

namespace ShopLibrary
{
    public class Shop
    {
        public static double Summ;
        public double Summ2;
        public static List<Product> basketlist = new List<Product>();
        public void Display() // вывод товаров 
        {
            Order order1 = new Order();
            //Обувь
            Console.WriteLine("Toп");
            var path = @"..\..\..\..\Shoes.csv";
            var lines = File.ReadAllLines(path);
            var shoes = new List<Shoes>();
            var BList = new List<Product>();
            for (var i = 0; i < lines.Length - 1; i++)
            {
                var splits = lines[i + 1].Split(';');
                var shoes1 = new Shoes();
                shoes1.Id = Convert.ToInt32(splits[0]);
                shoes1.Price = Convert.ToInt32(splits[1]);
                shoes1.Country = splits[2];
                shoes1.Color = splits[3];
                shoes1.Size = Convert.ToInt32(splits[4]);
                shoes1.LiningMaterial = splits[5];
                shoes1.OuterMaterial = splits[6];
                shoes1.Clasp = splits[7];
                shoes.Add(shoes1);
                BList.Add(shoes1);
                Console.WriteLine(shoes1);
            }
            Console.WriteLine("Обувь");


            //одежда
            var path2 = @"..\..\..\..\Clothes.csv";
            var lines2 = File.ReadAllLines(path2);
            var clothes = new List<Clothes>();

            for (var i = 0; i < lines2.Length - 1; i++)
            {
                var splits = lines2[i + 1].Split(';');
                var clothes1 = new Clothes();
                clothes1.Id = Convert.ToInt32(splits[0]);
                clothes1.Price = Convert.ToInt32(splits[1]);
                clothes1.Country = splits[2];
                clothes1.Color = splits[3];
                clothes1.Size = Convert.ToInt32(splits[4]);
                clothes1.Material = splits[5];
                clothes1.Clasp = splits[6];
                clothes.Add(clothes1);
                BList.Add(clothes1);
                Console.WriteLine(clothes1);
            }
            Console.WriteLine("Одежда");


            //Аксессуары
            var path3 = @"..\..\..\..\Accessories.csv";
            var lines3 = File.ReadAllLines(path3);
            var accessories = new List<Accessories>();

            for (var i = 0; i < lines3.Length - 1; i++)
            {
                var splits = lines3[i + 1].Split(';');
                var accessories1 = new Accessories();
                accessories1.Id = Convert.ToInt32(splits[0]);
                accessories1.Price = Convert.ToInt32(splits[1]);
                accessories1.Country = splits[2];
                accessories1.Color = splits[3];
                accessories1.Size = Convert.ToInt32(splits[4]);
                accessories1.Material = splits[5];
                accessories.Add(accessories1);
                BList.Add(accessories1);
                Console.WriteLine(accessories1);
            }
            Console.WriteLine("Аксессуары");
            PrId(BList);
        }


        static void PrId(List<Product> prod)// выбор товара по Id+сумма заказа 
        {
            int type = 1;
            User user = new User(10000);
            while (type == 1)
            {

                int index = 0;
                while (true)
                {
                    Console.Write("Введите Id товара: ");

                    var un = Convert.ToInt32(Console.ReadLine());


                    if ((!prod.Exists(x => x.Id == un)))
                    {
                        Console.Write("Такого Id нет, попробуйте ещё раз, нажмите 'Enter'");
                        Console.ReadLine();
                        Console.Clear();
                    }

                    else
                    {
                        index = prod.FindIndex(x => x.Id == un);
                        basketlist.Add(prod[index]);
                        Shop shop = new Shop();
                        Console.WriteLine(" 1-Продолжить покупку 2- Корзина наполнена ");
                        type = Convert.ToInt32(Console.ReadLine());
                        Summ = basketlist.Sum(x => x.Price);
                        if (type == 2)
                        {
                            var sum = basketlist.Sum(x => x.Price);
                            shop.Summ2 = sum;
                            Console.WriteLine("Сумма заказа: " + sum);
                            user.Notify += Account_Notify;
                            user.Take(sum);
                            Console.WriteLine($"Oстаток на карте { user.SumOnCard}");
                            return;
                        }
                    }
                }
            }
            return;
        }
        public static void Console_CancelKeyPress(object? sender, ConsoleCancelEventArgs e)

        {
            Console.WriteLine("Программа прервана, данные сохранены в файл");
            var result = "SaveOrder.txt";
            StreamWriter streamWriter = new StreamWriter(result);
            foreach (Product pr in basketlist)
            {
                Console.WriteLine(pr);
                streamWriter.WriteLine(pr);
            }
            streamWriter.WriteLine(Summ);
            streamWriter.Close();
            e.Cancel = true;

        }
        private static void Account_Notify(string message)
        {
            Console.WriteLine(message);
        }
    }
}
