﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopLibrary
{
    class Clothes : Product
    {
    public string Material { get; set; } //материал 
    public string Clasp { get; set; } //вид застежки 
    public Clothes(string material, string clasp, int id, int size, double price, string country, string color)
    {
        Material = material;
        Clasp = clasp;
        Id = id;
        Size = size;
        Price = price;
        Country = country;
        Color = color;
    }

    public Clothes()
    {
    }

    public override string ToString()
    {
        return $"Id:{Id} Материал:{Material} Вид застежки:{Clasp} Price:{Price} Country:{Country} Size:{Size} Color:{Color}";
    }

}
}
