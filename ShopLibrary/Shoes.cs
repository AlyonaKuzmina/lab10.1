﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopLibrary
{
    class Shoes : Product
    {
        public string LiningMaterial { get; set; } //материал подкладки 
        public string OuterMaterial { get; set; } //наружный материал
        public string Clasp { get; set; } //вид застежки
        public Shoes(string liningmaterial, string outerMaterial, string clasp, int id, int size, double price, string country, string color)
        {
            LiningMaterial = liningmaterial;
            OuterMaterial = outerMaterial;
            Clasp = clasp;
            Id = id;
            Size = size;
            Price = price;
            Country = country;
            Color = color;
        }

        public Shoes()
        {
        }

        public override string ToString()
        {
            return $"Id:{Id} Материал подкладки:{LiningMaterial} Наружный материал:{OuterMaterial} Вид застежки:{Clasp} Price:{Price} Country:{Country} Size:{Size} Color:{Color}";
        }
    }
}
