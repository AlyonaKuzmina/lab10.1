﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopLibrary
{
    public class User : User1
    {
        public string UserId = new(Guid.NewGuid().ToString("n").Substring(0, 18));
        public string Password { get; set; }
        public string Login { get; set; }
        public double SumOnCard { get; set; }

        //ввод пароля 
        public string CreatePasswors()
        {
            while (true)
            {
                Console.Write("Введите ваш пароль, не менее 5-ти символов: ");
                var pass = Console.ReadLine();
                if (pass.Length < 5)
                {
                    Console.Write("Недостаточно символов, попробуйте ещё раз, нажмите 'Enter'");
                    Console.ReadLine();
                    Console.Clear();
                }
                else return pass;
            }
        }

        public List<string> Tags { get; set; } = new();

        public delegate void AccountHandler(string message);
        public event AccountHandler? Notify;

        AccountHandler? notify;
        public event AccountHandler? NotifyWithManage
        {
            add
            {
                notify += value;
                Console.WriteLine($"{value.Method.Name} добавлен");
            }
            remove
            {
                notify -= value;
                Console.WriteLine($"{value.Method.Name} удален");
            }
        }


        public User(double sum)
        {
            SumOnCard = sum;
        }

        public void Take(double sum)
        {
            Order order = new Order();
            if (SumOnCard >= sum)
            {
                SumOnCard -= sum;
                Notify?.Invoke($"Со счета снято: {sum}");
                Console.WriteLine($" Спасибо за покупку! \n Ваш заказ придет  {order.DeliveryDate()}");

            }
            else
            {
                Notify?.Invoke($"Недостаточно денег на карте. Ваш баланс: {SumOnCard}");

            }
        }

    }
}
