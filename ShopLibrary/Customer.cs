﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopLibrary
{
    public class Customer : User
    {
        public Customer(double sum) : base(sum)
        {
        }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Email { get; set; }
        public int CreditCardInfo { get; set; }

    }
}
