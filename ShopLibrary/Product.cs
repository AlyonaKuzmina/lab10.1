﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopLibrary
{
    public class Product
    {
        public int Id { get; set; }
        public int Size { get; set; }
        public double Price { get; set; }
        public string Country { get; set; }
        public string Color { get; set; }

    }
}
